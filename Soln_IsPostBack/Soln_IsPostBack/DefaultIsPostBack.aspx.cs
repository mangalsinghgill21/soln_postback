﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Soln_IsPostBack
{
    public partial class DefaultIsPostBack : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Page.IsPostBack == false)
            {

                // Binding txtbox 
                txt1.Text = "Task";
                txt2.Text = "IsPostBack";
                txt3.Text = "Dynamic on";
                txt4.Text = "Button Click";


            }
        }
    }
}