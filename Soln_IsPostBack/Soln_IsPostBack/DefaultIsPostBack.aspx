﻿<%@ Page Language="C#" EnableViewState ="true" AutoEventWireup="true" CodeBehind="DefaultIsPostBack.aspx.cs" Inherits="Soln_IsPostBack.DefaultIsPostBack" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link href ="Css/txt.css"  type="text/css" rel="stylesheet"/>
</head>
<body>
    <form id="form1" runat="server">
         <div>
        <asp:TextBox ID="txt1" runat="server" EnableViewState ="true"  CssClass="txt"/>
        
        <asp:TextBox ID="txt2" runat="server" EnableViewState ="false" CssClass="txt"/>
        
        <asp:TextBox ID="txt3" runat="server" EnableViewState ="false"  CssClass="txt"/>
        
        <asp:TextBox ID="txt4" runat="server" EnableViewState ="true"  CssClass="txt"/>
        
        <asp:Button ID="btnsubmit" runat="server" Text="Submit" CssClass ="btn btn2 " />
         
         </div>
        
    </form>
</body>
</html>
